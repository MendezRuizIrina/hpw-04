
// Practica 4
//Funciones para la creación de un nuevo formulario
//Méndez Ruíz Irina

function crea_input(tip, nomb){
    obj_input=document.createElement("input");
    obj_input.setAttribute("type",tip);
    obj_input.setAttribute("name",nomb);
    return obj_input;
    }
crea_input("texto","textito");
    
//Creando la función Select
   
function crea_select(nom_select,difopc){
    
    obj_select=document.createElement("select");
    obj_select.setAttribute("id",nom_select);
var opcion = document.createElement("option");
    obj_select.appendChild(opcion);
    for(var j=0;j<difopc.length;j++){
variable = new Option(difopc[j]);
obj_select.option[j]=variable;}
      return obj_select;
}
crea_select ("select1",[5, 10, 15]);

//------------------Función , creando  <fieldset>-------------------------
    
function crea_fieldset(leyend,input_field){
    obj_fieldset=document.createElement("Fieldset"); 
    var leye=document.createElement("legend");
    leye.innerHTML=leyend;
    
obj_label=document.createElement("label");
obj_label.setAttribute("for","field");
    
field_input=document.createElement("input")
field_input.setAttribute("id",input_field);
    
obj_fieldset.appendChild(leye);
obj_fieldset.appendChild(obj_label);
obj_fieldset.appendChild(field_input);
    return obj_fieldset;}

crea_fieldset("leye","field");

//---------------creando <textarea></textarea>-----------------
function crea_Txarea(id,fil,colum){
    obj_text=document.createElement("textarea");
    obj_text.setAttribute("id",id,"fil",fil,"colum",colum);
    obj_text.setAttribute("fil",fil);
    obj_text.setAttribute("colum",colum);
return obj_text;}

crea_Txarea("txArea",5,15);
//-----------------creando <button>Button</button> -----------

function crea_botonsito(tipo,id,valor){
    obj_boton=document.createElement("button");
    obj_boton.setAttribute("tipo",tipo);
    obj_boton.setAttribute("valor",valor);
    obj_boton.setAttribute("id",id);
    return obj_boton;
}
crea_botonsito("botonmio","borrar","env");
